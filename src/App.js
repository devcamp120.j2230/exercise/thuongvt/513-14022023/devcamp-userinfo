
import './App.css';

import {gUserInfo } from './info';


function App() {
  return (
    <div >
      <img src={gUserInfo.avatar} alt="avt" width="300"/>
      <h5>Họ và tên User: {gUserInfo.firstname} {gUserInfo.lastname}</h5>
      <p>Tuổi của User là: {gUserInfo.age}</p>
      <p>{gUserInfo.age<=35 ? "Anh ấy còn trẻ":"Anh ấy đã già"}</p>
      <ul>
        {gUserInfo.language.map((value,index)=>{
          return <li key={index}>
            {value}
          </li>
        })}
      </ul>
    </div>
  );
}

export default App;
