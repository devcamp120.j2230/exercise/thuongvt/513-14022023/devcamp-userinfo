import img from "./assets/images/avatardefault_92824.png"
const gUserInfo = {
    firstname: 'Hoang',
    lastname: 'Pham',
    avatar: img,
    age: 30,
    language: ['Vietnamese', 'Japanese', 'English']
  }

  export {gUserInfo}
